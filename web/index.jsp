<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Parsing</title>
</head>
<body>
<h2>Choose type:</h2>

<form action="/parseservlet" method="post">
    <label>
        <select name="choice">
            <option value="sax">SAX</option>
            <option value="dom">DOM</option>
            <option value="stax">StAX</option>
        </select>
    </label><br/>
    <input type="submit" value="Parse">
</form>
</body>
</html>
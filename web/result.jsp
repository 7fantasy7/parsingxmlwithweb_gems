<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${message} Parser</title>
</head>
<body>

<h1>${message} Parser</h1>

<c:forEach var="gem" items="${result}">

    <c:choose>
        <c:when test="${'ru.botyanov.gem.entity.PreciousGem' eq gem['class'].name}" >
            <c:out value="PreciousGem"/>
        </c:when>
        <c:when test="${'ru.botyanov.gem.entity.SemiPreciousGem' eq gem['class'].name}" >
            <c:out value="SemiPreciousGem"/>
        </c:when>
    </c:choose>

    <table border="1">
        <tr>
            <th>Name</th>
            <th>Value</th>
        </tr>

        <tr>
            <td><b><c:out value="ID"/></b></td>
            <td><c:out value="${gem.id}"/></td>
        </tr>

        <tr>
            <td><b><c:out value="Name"/></b></td>
            <td><c:out value="${gem.name}"/></td>
        </tr>

        <tr>
            <td><b><c:out value="Origin"/></b></td>
            <td><c:out value="${gem.origin}"/></td>
        </tr>

        <tr>
            <td><b><c:out value="Value"/></b></td>
            <td><c:out value="${gem.value}"/></td>
        </tr>

        <c:choose>
            <c:when test="${'ru.botyanov.gem.entity.PreciousGem' eq gem['class'].name}" >
                <td><b><c:out value="Cut Style"/></b></td>
                <td><c:out value="${gem.cutStyle}"/></td>
            </c:when>
            <c:when test="${'ru.botyanov.gem.entity.SemiPreciousGem' eq gem['class'].name}" >
                <<td><b><c:out value="Polished"/></b></td>
                <td><c:out value="${gem.polished}"/></td>
            </c:when>
        </c:choose>

        <tr>
            <td colspan="2"><b><c:out value="Visual Parameters"/></b></td>
        </tr>

        <tr>
                <td><em><b><c:out value="Color"/></b></em></td>
                <td><c:out value="${gem.visualParameters.color}"/></td>
        </tr>

        <tr>
            <td><em><b><c:out value="Transparency"/></b></em></td>
            <td><c:out value=" ${gem.visualParameters.transparency}"/></td>
        </tr>

        <tr>
            <td><em><b><c:out value="Cuts"/></b></em></td>
            <td><c:out value=" ${gem.visualParameters.cuts}"/></td>
        </tr>

    </table>
    <br/>
</c:forEach>

<a href="index.jsp">Back to home page</a>
</body>
</html>

package ru.botyanov.gem.parser;

public class ParserFactory {
    private enum TypeParser {
        SAX, STAX, DOM
    }

    public static GemParser createDeviceBuilder(String typeParser) {
        TypeParser type = TypeParser.valueOf(typeParser.toUpperCase());
        switch (type) {
            case DOM:
                return new GemDOMBuilder();
            case SAX:
                return new GemSAXBuilder();
            case STAX:
                return new GemSTAXBuilder();
            default:
                throw new EnumConstantNotPresentException(
                        type.getDeclaringClass(), type.name());
        }
    }
}


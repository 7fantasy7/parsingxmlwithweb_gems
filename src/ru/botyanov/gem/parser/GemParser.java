package ru.botyanov.gem.parser;

import ru.botyanov.gem.entity.Gem;

import java.util.Set;

public abstract class GemParser {
    private Set<Gem> gems;

    public Set<Gem> getGems() {
        return gems;
    }

    public abstract void parse();
}
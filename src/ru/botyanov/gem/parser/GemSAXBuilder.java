package ru.botyanov.gem.parser;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import ru.botyanov.gem.entity.Gem;
import ru.botyanov.gem.main.Runner;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.HashSet;

public class GemSAXBuilder extends GemParser{
    private static final Logger LOG = Logger.getLogger(GemSAXBuilder.class);
    private static HashSet<Gem> gems = new HashSet<>();

    public void parse() {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser parser = factory.newSAXParser();
            GemHandler hfd = new GemHandler();
            parser.parse(Runner.FILE, hfd);
            gems = hfd.getGems();
        } catch (ParserConfigurationException | SAXException | IOException e) {
            LOG.error(e);
        }
    }

    public HashSet<Gem> getGems() {
        return gems;
    }
}
package ru.botyanov.gem.enums;

public enum Color {
    WHITE, YELLOW, SILVER, GOLDEN, CREAM, GREEN, BLACK, PINK, RED, BLUE, PURPLE, VIOLET, BROWN, GRAY, ORANGE
}
package ru.botyanov.gem.entity;

import ru.botyanov.gem.enums.CutStyle;

public class PreciousGem extends Gem {
    private CutStyle cutStyle;

    public PreciousGem() {
    }

    public PreciousGem(String name, String origin, double value,
                       VisualParameters visualParameters, CutStyle cutStyle, String id) {
        super(name, origin, value, visualParameters, id);
        this.cutStyle = cutStyle;
    }

    public CutStyle getCutStyle() {
        return cutStyle;
    }

    public void setCutStyle(CutStyle cutStyle) {
        this.cutStyle = cutStyle;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PreciousGem{");
        sb.append(super.toString());
        sb.append(", cutStyle=").append(cutStyle);
        sb.append('}');
        return sb.toString();
    }
}
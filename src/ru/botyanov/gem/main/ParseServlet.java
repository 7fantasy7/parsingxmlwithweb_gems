package ru.botyanov.gem.main;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import ru.botyanov.gem.parser.GemParser;
import ru.botyanov.gem.parser.ParserFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class ParseServlet extends HttpServlet {
    public static final Logger LOG = Logger.getLogger(ParseServlet.class);

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext context = config.getServletContext();
        String log4jLocation = context.getRealPath(config.getInitParameter("log4j-properties-location"));
        PropertyConfigurator.configure(log4jLocation);

        LOG.info("test");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doParse(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doParse(req, resp);
    }

    private void doParse(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String parserType = req.getParameter("choice");
        LOG.info(parserType);
        GemParser parser = ParserFactory.createDeviceBuilder(parserType);
        parser.parse();
        LOG.info(parser.getGems());
        req.setAttribute("result", parser.getGems());
        req.setAttribute("message", parserType);
        req.getRequestDispatcher("/result.jsp").forward(req, resp);
    }
}
